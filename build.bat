@echo off

set opts=-DGAME_WIN32=1
set code_path=%cd%\code\
IF NOT EXIST build (mkdir build)
pushd build

:: GENERAL COMPILER FLAGS
set compiler=           -nologo &:: Suppress Startup Banner
set compiler=%compiler% -Oi     &:: Use assembly intrinsics where possible
set compiler=%compiler% -MTd    &:: Include CRT library in the executable (static link of the debug version)
set compiler=%compiler% -Gm-    &:: Disable minimal rebuild
set compiler=%compiler% -GR-    &:: Disable runtime type info (C++)
set compiler=%compiler% -EHa-   &:: Disable exception handling (C++)
set compiler=%compiler% -W4     &:: Display warnings up to level 4
set compiler=%compiler% -WX     &:: Treat all warnings as errors

:: IGNORE WARNINGS
set compiler=           -wd4201 &:: Nameless struct/union
set compiler=           -wd4100 &:: Unused function parameter
set compiler=%compiler% -wd4189 &:: Local variable not referenced

:: DEBUG VARIABLES
set debug=        -FC &:: Produce the full path of the source code file
set debug=%debug% -Z7 &:: Produce debug information

:: CROSS_PLATFORM DEFINES
set defines=          -DGAME_INTERNAL=1 &:: Build for developer only (1) Build for public release (0)
set defines=%defines% -DGAME_SLOW=1     &:: Slow code welcome (1) No slow code allowed (0)

:: WIN32 PLATFORM LIBRARIES
set win32_libs=             user32.lib
set win32_libs=%win32_libs% gdi32.lib
set win32_libs=%win32_libs% winmm.lib

::COMMON LINKER SWITCHES
set link=       -opt:ref        &:: Remove unused functions
set link=%link% -incremental:no &:: Perform full link each time

:: DLL LINKER SWITCHES
set dll_link=           /EXPORT:GameUpdateAndRender
set dll_link=%dll_link% /EXPORT:GameGetSoundSamples

:: OPTIMIZATIONS
set optimization=%optimization% -Od &:: No optimizations (slow)
::set optimization=%optimization% -O2 &:: All optimizations (fast)

del *.pdb > NUL 2> NUL
cl %optimization% %compiler% %opts% %defines% %debug% -Fmgame.map %code_path%game.cpp -LD /link /pdb:game%random%.pdb %link% %dll_link%
cl %optimization% %compiler% %opts% %defines% %debug% -Fmwin32_game.map %code_path%win32_game.cpp %win32_libs% -Febuild /link %link% -subsystem:windows,5.2

popd
