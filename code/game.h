#if !defined(GAME_H)

/*
 NOTE(Jonas):

GAME_INTERNAL:
0 - Build for public release
1 - Build for developer only

GAME_SLOW:
0 - No slow code allowed!
1 - Slow code welcome.
*/

#include <math.h>
#include <stdint.h>

#define internal        static // Functions that, once declared, is available only in the specific file its declared in.
#define local_persist   static // Variables that, once declared, is available in the entirety of the scope it's in. It will be remembered if we exit and enter the scope again. DO NOT USE local_persist it is only here for later search and remove.
#define global_variable static // Variables that, once declared, is available in the entirety of the file it's in. Any function can access it, read its valiue.
#define Pi32 3.14159265359f

// unsigned integers
typedef int8_t  int8;  //s8;
typedef int16_t int16; //s16;
typedef int32_t int32; //s32;
typedef int64_t int64; //s64;

typedef uint8_t  uint8; //u8;
typedef uint16_t uint16; //u16;
typedef uint32_t uint32; //u32;
typedef uint64_t uint64; //u64;

typedef int32 bool32;

typedef float real32; //f32;
typedef double real64; //f64;

#define BYTES_PER_PIXEL 4

#if GAME_SLOW
#define Assert(Expression) if (!(Expression)) { *(int *)0 = 0; }
#else
#define Assert(Expression)
#endif

#define Kilobytes(Value) ((Value) * 1024LL)
#define Megabytes(Value) (Kilobytes(Value) * 1024LL)
#define Gigabytes(Value) (Megabytes(Value) * 1024LL)
#define Terabytes(Value) (Gigabytes(Value) * 1024LL)

#define Align16(val) ((val + 15) & ~15)
#define ArrayCount(Array) (sizeof(Array) / sizeof((Array)[0]))

inline uint32
SafeTruncateUInt64(uint64 value)
{
    Assert(value <= 0xFFFFFFFF);
    uint32 result = (uint32)value;
    return result;
}

#if GAME_INTERNAL
#define BEGIN_TIMED_BLOCK(id)                                                  \
u64 cycleStartCounter##id = __rdtsc();                                       \
debugGlobalMemory->counters[DebugCycleCounter_##id].name = #id;

#define END_TIMED_BLOCK(id)                                                    \
debugGlobalMemory->counters[DebugCycleCounter_##id].cycleCount               \
+= __rdtsc() - cycleStartCounter##id;                                      \
debugGlobalMemory->counters[DebugCycleCounter_##id].callCount++;

#define END_TIMED_BLOCK_COUNTED(id, count)                                     \
debugGlobalMemory->counters[DebugCycleCounter_##id].cycleCount               \
+= __rdtsc() - cycleStartCounter##id;                                      \
debugGlobalMemory->counters[DebugCycleCounter_##id].callCount += count;

#else

#define BEGIN_TIMED_BLOCK(id)
#define END_TIMED_BLOCK(id)

#endif

struct debug_cycle_counter {
    char *name;
    uint64 cycleCount;
    uint32 callCount;
};

struct game_offscreen_buffer
{
    // NOTE(Jonas): Pixels are always 32-bits wide, Memory Order BB GG RR XX
    void *memory;
    int width;
    int height;
    int pitch;
    int bytesPerPixel;
};

struct game_sound_output_buffer {
    int samplesPerSecond;
    int sampleCount;
    int toneVolume;
    int16 *memory;
};

struct game_button_state
{
    int32 halfTransitionCount;
    bool32 isDown;
};

struct game_controller_input
{
    bool32 isConnected;
    bool32 isAnalog;
    
    real32 stickAverageLX;
    real32 stickAverageLY;
    
    real32 stickAverageRX;
    real32 stickAverageRY;
    
    real32 leftTrigger;
    real32 rightTrigger;
    
    union
    {
        game_button_state buttons[14];
        struct 
        {
            // Controls
            game_button_state moveUp;
            game_button_state moveDown;
            game_button_state moveLeft;
            game_button_state moveRight;
            
            game_button_state actionUp;
            game_button_state actionDown;
            game_button_state actionLeft;
            game_button_state actionRight;
            
            game_button_state leftShoulder;
            game_button_state rightShoulder;
            game_button_state start;
            game_button_state back;
            
            game_button_state abilityBullet1; // TODO(Jonas): These are not buttons on the controller, så prop move?
            game_button_state abilityBullet2; // TODO(Jonas): These are not buttons on the controller, så prop move?
        };
    };
    
};

struct game_input
{
    real32 dt;
    game_button_state mouseButtons[5];
    int32 mouseX, mouseY, mouseZ;
    bool32 executableReloaded;
    
    game_controller_input controllers[5];
};

inline game_controller_input *GetController(game_input *Input, int ControllerIndex)
{
    Assert(ControllerIndex < ArrayCount(Input->controllers));
    game_controller_input *Result = &Input->controllers[ControllerIndex];
    return Result;
};

struct thread_context
{
    int placeholder;
};


struct platform_work_queue;
#define PLATFORM_WORK_QUEUE_CALLBACK(name)                                     \
void name(platform_work_queue *queue, void *data)

typedef PLATFORM_WORK_QUEUE_CALLBACK(platform_work_queue_callback);

typedef void platform_add_entry(platform_work_queue *queue,
                                platform_work_queue_callback *callback,
                                void *data);
typedef void platform_complete_all_work(platform_work_queue *queue);

#if GAME_INTERNAL
/* IMPORTANT(Jonas):
These are NOT for doing anything in the shipping game - they are blocking and the write doesn't protect against lost data.
*/
struct debug_read_file_result
{
    uint32 size;
    void *memory;
};

// NOTE(Jonas): Services that the game provides to the platform layer.
// #define DEBUG_PLATFORM_FREE_FILE_MEMORY(name) void name (thread_context *Thread, void *Memory)
#define DEBUG_PLATFORM_FREE_FILE_MEMORY(name) void name (thread_context *thread, void *memory)
typedef DEBUG_PLATFORM_FREE_FILE_MEMORY(debug_platform_free_file_memory);

#define DEBUG_PLATFORM_READ_ENTIRE_FILE(name) debug_read_file_result name (thread_context *thread, char *fileName)
typedef DEBUG_PLATFORM_READ_ENTIRE_FILE(debug_platform_read_entire_file);

#define DEBUG_PLATFORM_WRITE_ENTIRE_FILE(name) bool32 name (thread_context *thread, char *fileName, void *memory, uint32 fileSize)
typedef DEBUG_PLATFORM_WRITE_ENTIRE_FILE(debug_platform_write_entire_file);
#endif

struct game_memory
{
    uint64 permanentStorageSize;
    void *permanentStorage;
    uint64 transientStorageSize;
    void *transientStorage;
    bool32 isInitialized;
    
    platform_work_queue *highPriorityQueue;
    platform_add_entry *platformAddEntry;
    platform_complete_all_work *platformCompleteAllWork;
    
    debug_platform_free_file_memory *DEBUGPlatformFreeFileMemory;
    debug_platform_read_entire_file *DEBUGPlatformReadEntireFile;
    debug_platform_write_entire_file *DEBUGPlatformWriteEntireFile;
    
    debug_cycle_counter counters[32];
    
    bool32 isMatchDone; // 0 = match is ongoing; 1 = match is complete. NOTE(Jonas): This is properply not the correct location.
};

#define GAME_UPDATE_AND_RENDER(name) void name(thread_context *thread, game_memory *memory, game_input *input, game_input *oldInput, game_offscreen_buffer *buffer)
typedef GAME_UPDATE_AND_RENDER(game_update_and_render);

#define GAME_GET_SOUND_SAMPLES(name) void name(thread_context *thread, game_memory *memory, game_sound_output_buffer *soundBuffer)
typedef GAME_GET_SOUND_SAMPLES(game_get_sound_samples);

struct Ability
{
    // NOTE(Jonas): Consider the need for the initPos? Does the bullets need dissabere within a certain time?
    // Or is it enough with it hitting the wall all the time?
    int initPosX;
    int initPosY;
    int currentPosX;
    int currentPosY;
    int width;
    int headingX;
    int headingY;
    bool32 isActive;
};

struct Player
{
    int x;
    int y;
    int headingX;
    int headingY;
    Ability scan;
    
    int bulletIndex;
    Ability bullets[16];
    
    // NOTE(Jonas): Collider. The collider is currently a square, when this changes this needs to be updated.
    int colliderX1;
    int colliderY1;
    int colliderX2;
    int colliderY2;
    
    uint32 color;
};

struct game_state
{
    int toneHz;
    real32 tSine;
    
    // NOTE(Jonas): The offsets can be used to move the backgound.
    int xOffset;
    int yOffset;
    
    Player player1;
    Player player2;
    
    real32 playerSpeed;
};

#define GAME_H
#endif