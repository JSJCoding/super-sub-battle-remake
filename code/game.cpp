#include "game.h"

internal void
GameOutputSound(game_state *gameState, game_sound_output_buffer *soundBuffer, int toneHz)
{
    int16 toneVolume = 3000;
    int wavePeriod = soundBuffer->samplesPerSecond / toneHz;
    
    int16 *sampleOut = soundBuffer->memory;
    for (int sampleIndex = 0; sampleIndex < soundBuffer->sampleCount; ++sampleIndex)
    {
#if 0 // O - to disable sound and 1 - to enable sound.
        real32 sineValue = sinf(gameState->tSine);
        int16 sampleValue = (int16)(sineValue * toneVolume);
#else
        int16 sampleValue = 0;
#endif
        
        *sampleOut++ = sampleValue;
        *sampleOut++ = sampleValue;
        
        gameState->tSine += 2.0f * Pi32 * 1.0f / (real32)wavePeriod; // 1.0f could be running sample index.
        
        if(gameState->tSine > 2.0f * Pi32)
        {
            gameState->tSine -= 2.0f * Pi32;
        }
    }
}

internal void
RenderBackground(game_offscreen_buffer *buffer, int xOffset, int yOffset)
{
    
    uint8 *row = (uint8*)buffer->memory;
    for (int Y = 0; Y < buffer->height; ++Y)
    {
        uint32* pixel = (uint32*)row;
        for (int X = 0; X < buffer->width; ++X)
        {
            uint8 red = 0;
            uint8 green = (uint8)(Y + yOffset);
            uint8 blue = (uint8)(X + xOffset);
            
            *pixel++ = red << 16 | green << 8 | blue;
        }
        row += buffer->pitch;
    }
}


internal void
RenderPlayer(game_offscreen_buffer *buffer, Player player)
{
    int playerX = player.x;
    int playerY = player.y;
    int controllerX = player.headingX;
    int controllerY = player.headingY;
    
    uint8 *endOfBuffer = (uint8 *)buffer->memory + buffer->pitch * buffer->height;;
    int left = playerX;
    int right = playerX + 10;
    
    int top = playerY;
    int bottom = playerY + 10;
    
    for(int x = left; x < right; ++x)
    {
        uint8 *pixel = ((uint8 *)buffer->memory + x * buffer->bytesPerPixel + top * buffer->pitch);
        for(int y = top; y < bottom; ++y)
        {
            if((pixel >= buffer->memory) && (pixel < endOfBuffer))
            {
                *(uint32 *)pixel = player.color;
                pixel += buffer->pitch;
            }
        }
    }
    
    int startPosX = playerX + 5;
    int startPosY = playerY + 5;
    uint32 color = 0x00FF00FF;
    
    // Up
    if(controllerY > 0)
    {
        for(int y = startPosY; y >= playerY - controllerY * 10; --y)
        {
            uint8 *pixel = ((uint8 *)buffer->memory + startPosX * buffer->bytesPerPixel + y * buffer->pitch);
            if((pixel >= buffer->memory) && (pixel < endOfBuffer))
            {
                *(uint32 *)pixel = color;
                pixel += buffer->pitch;
            }
        }
    }
    
    // Down
    if(controllerY < 0)
    {
        for(int y = startPosY; y <= playerY + (controllerY * 10 * -1); ++y)
        {
            uint8 *pixel = ((uint8 *)buffer->memory + startPosX * buffer->bytesPerPixel + y * buffer->pitch);
            if((pixel >= buffer->memory) && (pixel < endOfBuffer))
            {
                *(uint32 *)pixel = color;
                pixel += buffer->pitch;
            }
        }
    }
    
    // Left
    if(controllerX < 0)
    {
        for(int x = startPosX; x >= playerX - (controllerX * 10 * -1); --x)
        {
            uint8 *pixel = ((uint8 *)buffer->memory + x * buffer->bytesPerPixel + startPosY * buffer->pitch);
            if((pixel >= buffer->memory) && (pixel < endOfBuffer))
            {
                *(uint32 *)pixel = color;
                pixel += buffer->pitch;
            }
        }
    }
    
    // Right
    if(controllerX > 0)
    {
        for(int x = startPosX; x <= playerX + controllerX * 10; ++x)
        {
            uint8 *pixel = ((uint8 *)buffer->memory + x * buffer->bytesPerPixel + startPosY * buffer->pitch);
            if((pixel >= buffer->memory) && (pixel < endOfBuffer))
            {
                *(uint32 *)pixel = color;
                pixel += buffer->pitch;
            }
        }
    }
    
    int startX1 = startPosX + controllerX;
    int startY1 = startPosY - controllerY;
    
    uint8 *pixel = ((uint8 *)buffer->memory + startX1 * buffer->bytesPerPixel + startY1 * buffer->pitch);
    
    if((pixel >= buffer->memory) && (pixel < endOfBuffer))
    {
        *(uint32 *)pixel = color;
        pixel += buffer->pitch;
    }
    
}

// NOTE(Jonas): This function draws a square border and is currently not used. It was used before the RenderCircle function was implemented. Maybe is can be used to render the map border. The map border is going to be a square at first.
internal int
RenderSquare(game_offscreen_buffer *buffer, int startX, int startY)
{
    uint8 *endOfBuffer = (uint8 *)buffer->memory + buffer->pitch * buffer->height;
    uint32 color = 0x00FF00FF;
    
    for(int x = startX; x <= buffer->width - 50; ++x)
    {
        uint8 *pixel = ((uint8 *)buffer->memory + x * buffer->bytesPerPixel + startY * buffer->pitch);
        for(int y = startY; y <= buffer->height - 50; ++y)
        {
            if((pixel >= buffer->memory) && (pixel < endOfBuffer))
            {
                if(x == startX || x == buffer->width - 50)
                {
                    *(uint32 *)pixel = color;
                }
                
                if(y == startY || y == buffer->height - 50)
                {
                    *(uint32 *)pixel = color;
                }
                pixel += buffer->pitch;
            }
        }
    }
    
    return 0;
}

internal int
RenderCircle(game_offscreen_buffer *buffer, int xCenter, int yCenter, int x, int y)
{
    uint32 color = 0x00FF00;
    
    uint8 *pixel = ((uint8 *)buffer->memory + (xCenter + x) * buffer->bytesPerPixel + (yCenter + y) * buffer->pitch);
    *(uint32 *)pixel = color;
    
    pixel = ((uint8 *)buffer->memory + (xCenter + x) * buffer->bytesPerPixel + (yCenter + y) * buffer->pitch);
    *(uint32 *)pixel = color;
    
    pixel = ((uint8 *)buffer->memory + (xCenter - x) * buffer->bytesPerPixel + (yCenter + y) * buffer->pitch);
    *(uint32 *)pixel = color;
    
    pixel = ((uint8 *)buffer->memory + (xCenter + x) * buffer->bytesPerPixel + (yCenter - y) * buffer->pitch);
    *(uint32 *)pixel = color;
    
    pixel = ((uint8 *)buffer->memory + (xCenter - x) * buffer->bytesPerPixel + (yCenter - y) * buffer->pitch);
    *(uint32 *)pixel = color;
    
    pixel = ((uint8 *)buffer->memory + (xCenter + y) * buffer->bytesPerPixel + (yCenter + x) * buffer->pitch);
    *(uint32 *)pixel = color;
    
    pixel = ((uint8 *)buffer->memory + (xCenter - y) * buffer->bytesPerPixel + (yCenter + x) * buffer->pitch);
    *(uint32 *)pixel = color;
    
    pixel = ((uint8 *)buffer->memory + (xCenter + y) * buffer->bytesPerPixel + (yCenter - x) * buffer->pitch);
    *(uint32 *)pixel = color;
    
    pixel = ((uint8 *)buffer->memory + (xCenter - y) * buffer->bytesPerPixel + (yCenter - x) * buffer->pitch);
    *(uint32 *)pixel = color;
    
    return 0;
}

// This function uses the Bresenhams Circle Algorithm
internal int
RenderCircle(game_offscreen_buffer *buffer, Ability *ability)
{
    if(ability->isActive == 0)
        return 0;
    
    if((ability->width * 2) >= 300)
    {
        ability->isActive = 0;
        return 0;
    }
    
    int x = 0;
    int y = ability->width; // this should be named radius.
    int d = 3 - 2 * ability->width;
    RenderCircle(buffer, ability->currentPosX, ability->currentPosY, x, y);
    while (y >= x)
    {
        ++x;
        if(d > 0)
        {
            --y;
            d = d + 4 * (x - y) + 10;
        }
        else
        {
            d = d + 4 * x + 6;
        }
        RenderCircle(buffer, ability->currentPosX, ability->currentPosY, x, y);
    }
    
    ability->width += 4;
    
    return 0;
}

internal int
RenderCircle(game_offscreen_buffer *buffer, int centerX, int centerY, int radius)
{
    int x = 0;
    int y = radius; // this should be named radius.
    int d = 3 - 2 * radius;
    RenderCircle(buffer, centerX, centerY, x, y);
    while (y >= x)
    {
        ++x;
        if(d > 0)
        {
            --y;
            d = d + 4 * (x - y) + 10;
        }
        else
        {
            d = d + 4 * x + 6;
        }
        RenderCircle(buffer, centerX, centerY, x, y);
    }
    
    return 0;
}

internal int
RenderBullet(game_offscreen_buffer *buffer, Ability *ability)
{
    for(int i = 0; i < 16 - 1; ++i)
    {
        if(ability[i].isActive == 0)
            continue;
        
        if(ability[i].currentPosX - ability[i].initPosX >= 300 || ability[i].initPosX - ability[i].currentPosX >= 300)
        {
            ability[i].isActive = 0;
            continue;
        }
        
        uint8 *endOfBuffer = (uint8 *)buffer->memory + buffer->pitch * buffer->height;
        uint32 color = 0x00FF00FF;
        int startPosX = ability[i].currentPosX;
        int startPosY = ability[i].currentPosY;
        
        // Render the bullet
        for(int x = startPosX; x <= ability[i].currentPosX + ability[i].width; ++x)
        {
            uint8 *pixel = ((uint8 *)buffer->memory + x * buffer->bytesPerPixel + startPosY * buffer->pitch);
            for(int y = startPosY; y <= ability[i].currentPosY + ability[i].width; ++y)
            {
                if((pixel >= buffer->memory) && (pixel < endOfBuffer))
                {
                    *(uint32 *)pixel = color;
                    pixel += buffer->pitch;
                }
            }
        }
        
        ability[i].currentPosX += ability[i].headingX > 0 ? ability[i].headingX : ability[i].headingX;
        ability[i].currentPosY += ability[i].headingY > 0 ? -ability[i].headingY : -ability[i].headingY;
    }
    
    return 0;
}

internal int
PlayerMovement(Player *player, int playerSpeed, int height, int width,
               bool32 up, bool32 down, bool32 left, bool32 right) // TODO(Jonas): Consider having a controler per player.
{
    if(player->y > 0 && up)
    {
        player->y -= (int)(playerSpeed * 1);
        player->headingY = 1;
        if(!left && !right)
            player->headingX = 0;
    }
    
    if(player->y + 10 < height && down)
    {
        player->y += (int)(playerSpeed * 1);
        player->headingY = -1;
        if(!left && !right)
            player->headingX = 0;
    }
    
    if(player->x > 0 && left)
    {
        player->x -= (int)(playerSpeed * 1);
        player->headingX = -1;
        if(!up && !down)
            player->headingY = 0;
    }
    
    if(player->x + 10 < width && right)
    {
        player->x += (int)(playerSpeed * 1);
        player->headingX = 1;
        if(!up && !down)
            player->headingY = 0;
    }
    
    player->colliderX1 = player->x;
    player->colliderY1 = player->y;
    player->colliderX2 = player->x + 10;
    player->colliderY2 = player->y + 10;
    
    return 0;
}

internal int
CheckCollisionScan(Player playerA, Player playerB)
{
    int dx = (playerA.x + 5) - (playerB.x + 5); // NOTE(Jonas): 5 is to get center of player. Change!
    int dy = (playerA.y + 5) - (playerB.y + 5);
    int distance = sqrt(dx * dx + dy * dy);
    bool32 isColliding = distance < playerA.scan.width + 5; // NOTE(Jonas): 5 is the player radius.
    if(isColliding)
    {
        playerB.color = 0x0000FF00;
        isColliding = 0;
        return 1;
    }
    return 0;
}

internal int
CheckCollisionBullets(Player playerA, Player playerB)
{
    for(int i = 0; i < 16; ++i)
    {
        if(playerA.bullets[i].isActive == 0)
            continue;
        
        int dx = (playerA.bullets[i].currentPosX + 2) - (playerB.x + 5);
        int dy = (playerA.bullets[i].currentPosY + 2) - (playerB.y + 5);
        int distance = sqrt(dx * dx + dy * dy);
        bool32 isColliding = distance < playerA.bullets[i].width + 5; // NOTE(Jonas): 5 is the player radius.
        if(isColliding)
        {
            return 1;
        }
    }
    return 0;
}

#if defined __cplusplus
extern "C"
#endif
GAME_UPDATE_AND_RENDER(GameUpdateAndRender)
{
    Assert(sizeof(game_state) <= memory->permanentStorageSize);
    
    game_state *gameState = (game_state *)memory->permanentStorage;
    
    if(!memory->isInitialized)
    {
        gameState->xOffset = 0;
        gameState->yOffset = 0;
        gameState->toneHz = 256;
        gameState->player1.x = 100;
        gameState->player1.y = buffer->height / 2;
        gameState->player1.colliderX1 = gameState->player1.x;
        gameState->player1.colliderY1 = gameState->player1.y;
        gameState->player1.colliderX2 = gameState->player1.x + 10;
        gameState->player1.colliderY2 = gameState->player1.y + 10;
        gameState->player1.color = 0x00FFFFFF;
        
        gameState->player2.x = buffer->width - 110; // NOTE(Jonas): The extra 10px is the width of the player.
        gameState->player2.y = buffer->height / 2;
        gameState->player2.colliderX1 = gameState->player2.x;
        gameState->player2.colliderY1 = gameState->player2.y;
        gameState->player2.colliderX2 = gameState->player2.x + 10;
        gameState->player2.colliderY2 = gameState->player2.y + 10;
        gameState->player2.color = 0x00FFFFFF;
        
        gameState->playerSpeed = 5.0f;
        memory->isInitialized = true;
        
        debug_read_file_result fileData = memory->DEBUGPlatformReadEntireFile(thread, __FILE__);
        if (fileData.memory)
        {
            memory->DEBUGPlatformWriteEntireFile(thread, "test.out", fileData.memory, fileData.size);
            memory->DEBUGPlatformFreeFileMemory(thread, fileData.memory);
        }
    }
    
    //
    // Collision Detection
    // 
    
    if(memory->isMatchDone == 0)
        memory->isMatchDone = CheckCollisionBullets(gameState->player1, gameState->player2);
    
    if(memory->isMatchDone == 0)
        memory->isMatchDone = CheckCollisionBullets(gameState->player2, gameState->player1);
    
    if(gameState->player1.scan.isActive)
        CheckCollisionScan(gameState->player1, gameState->player2);
    
    if(gameState->player2.scan.isActive)
        CheckCollisionScan(gameState->player2, gameState->player1);
    
    // 
    // Input processing
    //
    for (int controllerIndex = 0; controllerIndex < ArrayCount(input->controllers); ++controllerIndex)
    {
        game_controller_input *controller = GetController(input, controllerIndex);
        
        if(controller->isAnalog)
        {
            gameState->toneHz = 256 + (int)(128.0f * (controller->stickAverageLY));
            
            // Use analog movement tuning.
            real32 LX = controller->stickAverageLX;
            real32 LY = controller->stickAverageLY;
            real32 RX = controller->stickAverageRX;
            real32 RY = controller->stickAverageRY;
            
            {
                int dx = (gameState->player1.x + 5 + (int)(gameState->playerSpeed * LX)) - (buffer->width/2);
                int dy = (gameState->player1.y + 5 - (int)(gameState->playerSpeed * LY)) - (buffer->height/2);
                int distance = sqrt(dx * dx + dy * dy);
                bool32 isColliding = distance <= 5 + 100; // NOTE(Jonas): 5 is the player radius.
                if(!isColliding)
                {
                    if(gameState->player1.x + (int)(gameState->playerSpeed * LX) <= buffer->width - 50 && gameState->player1.x + (int)(gameState->playerSpeed * LX) >= 50 &&
                       gameState->player1.y - (int)(gameState->playerSpeed * LY) <= buffer->height - 50 && gameState->player1.y - (int)(gameState->playerSpeed * LY) >= 50)
                    {
                        gameState->player1.x += (int)(gameState->playerSpeed * LX);
                        gameState->player1.y -= (int)(gameState->playerSpeed * LY);
                        
                        gameState->player1.colliderX1 = gameState->player1.x;
                        gameState->player1.colliderY1 = gameState->player1.y;
                        gameState->player1.colliderX2 = gameState->player1.x + 10;
                        gameState->player1.colliderY2 = gameState->player1.y + 10;
                        
                        if(LX != 0 || LY != 0)
                        {
                            gameState->player1.headingX = (int)(LX * 10);
                            gameState->player1.headingY = (int)(LY * 10);
                        }
                    }
                }
            }
            
            {
                int dx = (gameState->player2.x + 5 + (int)(gameState->playerSpeed * RX)) - (buffer->width/2);
                int dy = (gameState->player2.y + 5 - (int)(gameState->playerSpeed * RY)) - (buffer->height/2);
                int distance = sqrt(dx * dx + dy * dy);
                bool32 isColliding = distance <= 5 + 100; // NOTE(Jonas): 5 is the player radius.
                if(!isColliding)
                {
                    if(gameState->player2.x + 10 + (int)(gameState->playerSpeed * RX) <= buffer->width - 50 && gameState->player2.x + 10 + (int)(gameState->playerSpeed * RX) >= 50 &&
                       gameState->player2.y + 10 - (int)(gameState->playerSpeed * RY) <= buffer->height - 50 && gameState->player2.y + 10 - (int)(gameState->playerSpeed * RY) >= 50)
                    {
                        gameState->player2.x += (int)(gameState->playerSpeed * RX);
                        gameState->player2.y -= (int)(gameState->playerSpeed * RY);
                        
                        gameState->player2.colliderX1 = gameState->player2.x;
                        gameState->player2.colliderY1 = gameState->player2.y;
                        gameState->player2.colliderX2 = gameState->player2.x + 10;
                        gameState->player2.colliderY2 = gameState->player2.y + 10;
                        
                        if(RX != 0 || RY != 0)
                        {
                            gameState->player2.headingX = (int)(RX * 10);
                            gameState->player2.headingY = (int)(RY * 10);
                        }
                    }
                }
            }
        }
        
        if(controller->moveUp.isDown || controller->moveDown.isDown || 
           controller->moveLeft.isDown || controller->moveRight.isDown)
            PlayerMovement(&gameState->player1, gameState->playerSpeed, buffer->height, buffer->width,
                           controller->moveUp.isDown, controller->moveDown.isDown, controller->moveLeft.isDown, controller->moveRight.isDown);
        
        if(controller->actionUp.isDown || controller->actionDown.isDown ||
           controller->actionLeft.isDown || controller->actionRight.isDown)
            PlayerMovement(&gameState->player2, gameState->playerSpeed, buffer->height, buffer->width,
                           controller->actionUp.isDown, controller->actionDown.isDown, controller->actionLeft.isDown, controller->actionRight.isDown);
        
        {
            Player *player = {};
            if(controller->leftShoulder.isDown && gameState->player1.scan.isActive == 0)
                player = &gameState->player1;
            
            if(controller->rightShoulder.isDown && gameState->player2.scan.isActive == 0)
                player = &gameState->player2;
            
            if(player != NULL)
            {
                player->scan.initPosX = player->x + 5;
                player->scan.initPosY = player->y + 5;
                player->scan.currentPosX = player->scan.initPosX;
                player->scan.currentPosY = player->scan.initPosY;
                player->scan.width = 10;
                player->scan.isActive = 1;
            }
        }
        
        {
            Player *player = &gameState->player1;
            if(controller->leftTrigger > 0 || controller->abilityBullet1.isDown)
            {
                int bulletIndex = player->bulletIndex;
                
                player->bullets[bulletIndex].initPosX = player->x + 2;
                player->bullets[bulletIndex].initPosY = player->y + 2;
                player->bullets[bulletIndex].currentPosX = player->bullets[bulletIndex].initPosX;
                player->bullets[bulletIndex].currentPosY = player->bullets[bulletIndex].initPosY;
                player->bullets[bulletIndex].width = 5;
                player->bullets[bulletIndex].headingX = player->headingX;
                player->bullets[bulletIndex].headingY = player->headingY;
                player->bullets[bulletIndex].isActive = 1;
                
                if(bulletIndex < sizeof(player->bullets)/sizeof(Ability) - 1)
                    ++player->bulletIndex;
                else
                    player->bulletIndex = 0;
            }
            
            player = &gameState->player2;
            if(controller->rightTrigger > 0 || controller->abilityBullet2.isDown)
            {
                int bulletIndex = player->bulletIndex;
                
                player->bullets[bulletIndex].initPosX = player->x + 2;
                player->bullets[bulletIndex].initPosY = player->y + 2;
                player->bullets[bulletIndex].currentPosX = player->bullets[bulletIndex].initPosX;
                player->bullets[bulletIndex].currentPosY = player->bullets[bulletIndex].initPosY;
                player->bullets[bulletIndex].width = 5;
                player->bullets[bulletIndex].headingX = player->headingX;
                player->bullets[bulletIndex].headingY = player->headingY;
                player->bullets[bulletIndex].isActive = 1;
                
                if(bulletIndex < sizeof(player->bullets)/sizeof(Ability) - 1)
                    ++player->bulletIndex;
                else
                    player->bulletIndex = 0;
            }
        }
    }
    
    // 
    // Rendering
    //
    RenderBackground(buffer, gameState->xOffset, gameState->yOffset);
    RenderSquare(buffer, 50, 50);
    RenderCircle(buffer, buffer->width/2, buffer->height/2, 100);
    
    RenderPlayer(buffer, gameState->player1);
    RenderPlayer(buffer, gameState->player2);
    
    RenderCircle(buffer, &gameState->player1.scan);
    RenderCircle(buffer, &gameState->player2.scan);
    
    RenderBullet(buffer, gameState->player1.bullets);
    RenderBullet(buffer, gameState->player2.bullets);
}

#if defined __cplusplus
extern "C"
#endif
GAME_GET_SOUND_SAMPLES(GameGetSoundSamples)
{
    game_state *gameState = (game_state*)memory->permanentStorage;
    GameOutputSound(gameState, soundBuffer, gameState->toneHz);
}