#if !defined(WIN32_GAME_H)
// Bitmap offscree buffer
struct win32_offscreen_buffer
{
    // NOTE(jonas): Pixels are always 32-bits wide, 
    // Memory Order  0x BB GG RR xx
    // Little Endian 0x xx RR GG BB
    BITMAPINFO info;
    void *memory;
    //int Width;
    int width;
    // int Height;
    int height;
    int pitch;
    int bytesPerPixel;
};

struct win32_window_dimension
{
    int width;
    int height;
};

struct win32_sound_output
{
    int samplesPerSecond;
    uint32 runningSampleIndex;
    int bytesPerSample;
    int secondaryBufferSize;
    DWORD safetyBytes;
    DWORD bufferSize;
    int16 toneVolume;
    // TODO(Jonas): Math gets simpler if we add a "bytes per second" field?
    
};

struct win32_debug_sound_marker
{
    DWORD outputPlayCursor;
    DWORD outputWriteCursor;
    DWORD outputLocation;
    DWORD outputByteCount;
    DWORD expectedFlipPlayCursor;
    DWORD lockOffset;
    DWORD flipPlayCursor;
    DWORD flipWriteCursor;
    DWORD bytesToLock;
};

struct win32_game_code
{
    // HMODULE GameCodeDLL;
    HMODULE gameDLL;
    FILETIME gameDLLLastWriteTime;
    bool32 isValid;
    
    // IMPORTANT(Jonas): Either of the callbacks can be 0!
    // You must check before calling
    //game_update_and_render *UpdateAndRender;
    //game_get_sound_samples *GetSoundSamples;
    game_update_and_render *gameUpdateVideo; // Alternative name: game_update_video
    game_get_sound_samples *gameUpdateAudio; // Alternative name: game_update_sound
    
    bool32 IsValid;
};

struct win32_recorded_input
{
    int InputCount;
    game_input *InputStream;
};

// #define WIN32_STATE_FILENAME_COUNT MAX_PATH
#define WIN32_MAX_PATH MAX_PATH
struct win32_replay_buffer
{
    HANDLE FileHandle;
    HANDLE MemoryMap;
    //char Filename[WIN32_STATE_FILENAME_COUNT];
    char Filename[WIN32_MAX_PATH];
    // void *MemoryBlock;
    void *memory;
    HANDLE inputHandle; // This comes from the Github, what does it do?
};

struct win32_state
{
    uint64 totalSize;
    // void *GameMemoryBlock;
    void *gameMemory;
    uint64 memorySize;
    
    //win32_replay_buffer ReplayBuffers[4];
    win32_replay_buffer replayBuffers[4];
    
    HANDLE RecordingHandle;
    // int InputRecordingIndex;
    int inputRecordingIndex;
    
    HANDLE PlaybackHandle;
    //int InputPlayingIndex;
    int inputPlayingBackIndex;
    
    //char EXEFilename[MAX_PATH];
    char exePath[MAX_PATH];
    int exeDirLength; // This was used in the github version, what to do with it?
    
    char *OnePastLastEXEFilenameSlash;
};

#define WIN32_GAME_H
#endif