#include <windows.h>
#include <stdint.h>

#define internal        static // Is a function that, once declared, is available only in the specific file its declared in.
#define local_persist   static // Is a variable that, once declared, is available in the entirety of the scope it's in. It will be remembered if we exit and enter the scope again. DO NOT USE local_persist it is only here for later search and remove.
#define global_variable static // Is a variable that, once declared, is available in the entirety of the file it's in. Any function can access it, read its valiue.

// unsigned integers
typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t  s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;

// Bitmap offscree buffer
struct win32_offscreen_buffer
{
    // NOTE(jonas): Pixels are always 32-bits wide, 
    // Memory Order  0x BB GG RR xx
    // Little Endian 0x xx RR GG BB
    BITMAPINFO Info;
    void *Memory;
    int Width;
    int Height;
    int Pitch;
    int BytesPerPixel;
};

struct win32_window_dimension
{
    int Width;
    int Height;
};

global_variable bool GlobalRunning;
global_variable win32_offscreen_buffer GlobalBackbuffer;

internal win32_window_dimension
Win32GetWindowDimension(HWND Window)
{
    win32_window_dimension Result;
    
    RECT ClientRect;
    GetClientRect(Window, &ClientRect);
    Result.Width = ClientRect.right - ClientRect.left;
    Result.Height = ClientRect.bottom - ClientRect.top;
    
    return Result;
}

internal void
RenderWeirdGradient(win32_offscreen_buffer *Buffer, int XOffset, int YOffset)
{
    
    u8 *Row = (u8*)Buffer->Memory;
    for (int Y = 0; Y < Buffer->Height; ++Y)
    {
        u32* Pixel = (u32*)Row;
        for(int X = 0; X < Buffer->Width; ++X)
        {
            u8 Red = 0;
            u8 Green = (u8)(Y + YOffset);
            u8 Blue = (u8)(X + XOffset);
            
            *Pixel++ = Red << 16 | Green << 8 | Blue;
        }
        Row += Buffer->Pitch;
    }
}

// Resize Device Independent Bitmap (DIB)
internal void
Win32ResizeDIBSection(win32_offscreen_buffer *Buffer, int Width, int Height)
{
    // This condition is only neccesary in the case where we call this function more than once. We don't but just in case.
    if (Buffer->Memory)
    {
        VirtualFree(Buffer->Memory, 0, MEM_RELEASE);
    }
    
    Buffer->Width = Width;
    Buffer->Height = Height;
    Buffer->BytesPerPixel = 4; // RGB only requires 3 bytes per pixel the last byte is just for padding.
    Buffer->Pitch = Buffer->Width * Buffer->BytesPerPixel;
    
    Buffer->Info.bmiHeader.biSize = sizeof(Buffer->Info.bmiHeader); // Size of the bmiHeader structure.
    Buffer->Info.bmiHeader.biWidth = Buffer->Width;                 // Width of the DIB section.
    Buffer->Info.bmiHeader.biHeight = -Buffer->Height;              // Height of the DIB section. Negative value: top-down pitch. meaning that the first bytes of the image are the color for the top left pixel in the bitmap, not the bottom left.
    Buffer->Info.bmiHeader.biPlanes = 1;                            // This is an obsolete value which by now should always be 1.
    Buffer->Info.bmiHeader.biBitCount = 32;                         // The number of bits per pixel. We use bytes 3 for RGB = 24 bits. 8 bits per color. 
    Buffer->Info.bmiHeader.biCompression = BI_RGB;                  // What type of compression we want. BI_RGB --> no compression.
    
    
    int BitmapMemorySize = Buffer->BytesPerPixel * (Buffer->Width * Buffer->Height); // The area of a rect time the number of bytes per pixel.
    Buffer->Memory = VirtualAlloc(0, BitmapMemorySize, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
}

internal void
Win32DisplayBufferInWindow(win32_offscreen_buffer *Buffer, HDC DeviceContext, int WindowWidth, int WindowHeight)
{
    // TODO(jonas): Aspect ratio correction.
    
    StretchDIBits(DeviceContext,
                  0, 0, WindowWidth, WindowHeight,     // destionation rectable (Window)
                  0, 0, Buffer->Width, Buffer->Height, // source rectangle (bitmap buffer)
                  Buffer->Memory,
                  &Buffer->Info,
                  DIB_RGB_COLORS, SRCCOPY);
}

// Passes message information to the specified window procedure. And will be called any time windows needs to send someting to a window of this class (our WindowClass) to have it do something.
LRESULT CALLBACK
Win32MainWindowCallBack(HWND Window,   // A handle to the window procedure to receive the message.
                        UINT Message,  // The message.
                        WPARAM wParam, // Additional message-specific information. The contents of this parameter depend on the value of the Msg parameter.
                        LPARAM lParam) // Additional message-specific information. The contents of this parameter depend on the value of the Msg parameter.
{
    LRESULT Result = 0;
    switch (Message)
    {
        case WM_SIZE: 
        {
        } break;
        
        case WM_DESTROY:
        {
            Running = false;
        } break;
        
        case WM_CLOSE:
        {
            Running = false;
        } break;
        
        case WM_PAINT:
        {
            PAINTSTRUCT Paint;
            HDC DeviceContext = BeginPaint(Window, &Paint);
            win32_window_dimension Dimension = Win32GetWindowDimension(Window);
            Win32DisplayBufferInWindow(&GlobalBackbuffer, DeviceContext, Dimension.Width, Dimension.Height);
            EndPaint(Window, &Paint);
        } break;
        
        case WM_ACTIVATEAPP:
        {
            OutputDebugStringA("WM_ACTIVATEAPP\n");
        } break;
        
        default:
        {
            // The default window procedure to provide default processing for any window messages that an application does not process.
            // This function ensures that every message is processed. DefWindowProc is called with the same parameters received by the window procedure.
            Result = DefWindowProc(Window, Message, wParam, lParam);
        } break;
    }
    return Result;
}

int CALLBACK
WinMain(
        HINSTANCE Instance,     // A handle to the current instance of the application.
        HINSTANCE PrevInstance, // A handle to the previous instance of the application.
        LPSTR     CommandLine,  // The command line for the applicaiotn, excluding the program name.
        int       ShowCode)     // Controls how the window is to be shown.
{
    WNDCLASSA WindowClass = {};
    
    // CS_VREDRAW: Redraws the entire window if movement or resizing changes the width/height of the client area.
    // TODO(Jonas): Check if HREDRAW/VREDRAW/OWNDC still matters.
    WindowClass.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW; 
    WindowClass.lpfnWndProc = Win32MainWindowCallBack;  // A pointer to the window function that defines how the window responds to events.
    WindowClass.hInstance = Instance;                   // The instance handler of the currently running code.
    // WindowClass.hIcon = ;                            // Handle for our game icon.
    WindowClass.lpszClassName = "GameWindowClass";      // Name for the Window Class.
    
    // Regisers a window class for subsequent use in calls to CreateWindow or CreateWindowEx function.
    if (RegisterClassA(&WindowClass))
    {
        HWND Window = CreateWindowExA(0,
                                      WindowClass.lpszClassName,
                                      "GameName",
                                      WS_OVERLAPPEDWINDOW | WS_VISIBLE,
                                      CW_USEDEFAULT,
                                      CW_USEDEFAULT,
                                      CW_USEDEFAULT,
                                      CW_USEDEFAULT,
                                      0,
                                      0,
                                      Instance,
                                      0);
        if (Window)
        {
            // Window creation successful!
            Running = true;
            int XOffset = 0;
            int YOffset = 0;
            Win32ResizeDIBSection(&GlobalBackbuffer, 1280, 720);
            while(GlobalRunning)
            {
                // NOTE(Jonas): Since we specifued CS_OWNDC, we can just get one device context and use it forever because
                // we are not shareing it with anyone.
                HDC DeviceContext = GetDC(Window);
                MSG Message;
                while(PeekMessageA(&Message, 0, 0, 0, PM_REMOVE))
                {
                    // Do work in your window.
                    if(Message.message == WM_QUIT)
                    {
                        Running = false;
                    }
                    TranslateMessage(&Message);  // Used to "translate" Windows Virtual key character codes to actual char symbols.
                    DispatchMessageA(&Message);
                }
                
                RenderWeirdGradient(&GlobalBackbuffer, XOffset, YOffset);
                ++XOffset;
                
                win32_window_dimension Dimension = Win32GetWindowDimension(Window);
                Win32DisplayBufferInWindow(&GlobalBackbuffer, DeviceContext, Dimension.Width, Dimension.Height);
            }
            // Exit procedures
        }
        else
        {
            // Window Creation failed!
            // TODO(Jonas): Logging
        }
    }
    else
    {
        // Window Class Registration failed!
        // TODO(Jonas): Logging
    }
    
    return 0;
}
