# Super Sub Battle Remake

## Project status
It is currently a work in progress and in the very early stages. I am currently focusing on getting the gameplay loop up and running.

## Description
Super Sub Battle Remake is a recreation of the original game [Super Sub Battle](https://gitlab.com/JSJCoding/super-sub-battle) made in 48 hours during the participation of a course by the Youth Science Association (UNF) back in 2018. You play as two submarines and are trying to destroy each other. The gimmick is that both submarines are invisible while traversing the map. If you lose your bearings the submarines are equipped with sonars, these can be used to locate yourself and the enemy, but be careful the sonars also will reveal your location to the enemy.

## Current state
- [x] Get the windows window up and running
- [x] Modify specific pixel in window
- [x] Implement keyboard controller
- [x] Implement Xbox controller
- [x] Implement FPS limiter
- [x] Implement movement controls
- [x] Implement terrain collisions
- [x] Implement ability bullet
- [ ] Implement ability scan (work in progress)
- [ ] Implement game sound
- [x] Implement input recording and playback
- [ ] Implement match start countdown
- [ ] Implement match start player fadeaway during countdown
- [ ] Implement UI Buttons
- [ ] Implement UI Text
- [ ] Implement start screen
- [ ] Implement winning screen
- [ ] Implement rematch functionality
- [ ] Implement settings screen (Keybindings)
- [ ] Implement better visuals (current not certain of how its gonna look).

[x] = complete and [ ] = future work.
NOTE: This current state list considers the basic implementation.
