#include "game.h"

internal void
GameOutputSound(game_state *GameState, game_sound_output_buffer *SoundBuffer, int ToneHz)
{
    int16 ToneVolume = 3000;
    int WavePeriod = SoundBuffer->SamplesPerSecond / ToneHz;
    
    int16 *SampleOut = SoundBuffer->Samples;
    for (int SampleIndex = 0; SampleIndex < SoundBuffer->SampleCount; ++SampleIndex)
    {
#if 1 // O - to disable sound and 1 - to enable sound.
        real32 SineValue = sinf(GameState->tSine);
        int16 SampleValue = (int16)(SineValue * ToneVolume);
#else
        int16 SampleValue = 0;
#endif
        
        *SampleOut++ = SampleValue;
        *SampleOut++ = SampleValue;
        
        GameState->tSine += 2.0f * Pi32 * 1.0f / (real32)WavePeriod; // 1.0f could be running sample index.
        
        if(GameState->tSine > 2.0f * Pi32)
        {
            GameState->tSine -= 2.0f * Pi32;
        }
    }
}

internal void
RenderWeirdGradient(game_offscreen_buffer *Buffer, int XOffset, int YOffset)
{
    
    uint8 *Row = (uint8*)Buffer->Memory;
    for (int Y = 0; Y < Buffer->Height; ++Y)
    {
        uint32* Pixel = (uint32*)Row;
        for (int X = 0; X < Buffer->Width; ++X)
        {
            uint8 Red = 0;
            uint8 Green = (uint8)(Y + YOffset);
            uint8 Blue = (uint8)(X + XOffset);
            
            *Pixel++ = Red << 16 | Green << 8 | Blue;
        }
        Row += Buffer->Pitch;
    }
}


internal void
RenderPlayer(game_offscreen_buffer *Buffer, int PlayerX, int PlayerY)

{
    uint8 *EndOfBuffer = (uint8 *)Buffer->Memory + Buffer->Pitch * Buffer->Height;
    uint32 color = 0xFFFFFFFF;
    int left = PlayerX;
    int right = PlayerX + 10;
    
    int top = PlayerY;
    int bottom = PlayerY + 10;
    for(int x = left; x < right; ++x)
    {
        uint8 *Pixel = ((uint8 *)Buffer->Memory + x * Buffer->BytesPerPixel + top * Buffer->Pitch);
        for(int y = top; y < bottom; ++y)
        {
            if((Pixel >= Buffer->Memory) && (Pixel < EndOfBuffer))
            {
                *(uint32 *)Pixel = color;
                Pixel += Buffer->Pitch;
            }
        }
    }
}

#if defined __cplusplus
extern "C"
#endif
GAME_UPDATE_AND_RENDER(GameUpdateAndRender)
{
    Assert(sizeof(game_state) <= Memory->PermanentStorageSize);
    
    game_state *GameState = (game_state *)Memory->PermanentStorage;
    if(!Memory->IsInitialized)
    {
        GameState->XOffset = 0;
        GameState->YOffset = 0;
        GameState->ToneHz = 512;
        GameState->PlayerX = 100;
        GameState->PlayerY = 100;
        Memory->IsInitialized = true;
        
        debug_read_file_result FileData = Memory->DEBUGPlatformReadEntireFile(Thread, __FILE__);
        if (FileData.Contents)
        {
            Memory->DEBUGPlatformWriteEntireFile(Thread, "test.out", FileData.ContentsSize, FileData.Contents);
            Memory->DEBUGPlatformFreeFileMemory(Thread, FileData.Contents);
        }
    }
    
    for (int ControllerIndex = 0; ControllerIndex < ArrayCount(Input->Controllers); ++ControllerIndex)
    {
        game_controller_input *Controller = GetController(Input, ControllerIndex);
        if (Controller->IsAnalog)
        {
            // Use analog movement tuning.
            GameState->XOffset += (int)(4.0f * Controller->StickAverageX);
            GameState->ToneHz = 512 + (int)(128.0f * (Controller->StickAverageY));
        }
        
        GameState->PlayerX += (int)(4.0f * Controller->StickAverageX);
        GameState->PlayerY -= (int)(4.0f * Controller->StickAverageY);
        if(GameState->tJump > 0)
        {
            GameState->PlayerY += (int)(10.0f * sinf(0.5f * Pi32 * GameState->tJump));
        }
        if(Controller->ActionDown.EndedDown)
        {
            GameState->tJump = 4.0;
        }
        GameState->tJump -= 0.033f;
        
    }
    
    RenderWeirdGradient(Buffer, GameState->XOffset, GameState->YOffset);
    RenderPlayer(Buffer, GameState->PlayerX, GameState->PlayerY);
    
    for(int ButtonIndex = 0; ButtonIndex < ArrayCount(Input->MouseButtons); ++ButtonIndex)
    {
        if(Input->MouseButtons[ButtonIndex].EndedDown)
        {
            RenderPlayer(Buffer, 10 + 20 * ButtonIndex, 10);
        }
    }
    
    RenderPlayer(Buffer, Input->MouseX, Input->MouseY);
}

#if defined __cplusplus
extern "C"
#endif
GAME_GET_SOUND_SAMPLES(GameGetSoundSamples)
{
    game_state *GameState = (game_state*)Memory->PermanentStorage;
    GameOutputSound(GameState, SoundBuffer, GameState->ToneHz);
}